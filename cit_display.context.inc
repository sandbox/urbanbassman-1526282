<?php
/**
 * @file
 * cit_display.context.inc
 */

/**
 * Implementation of hook_context_default_contexts().
 */
function cit_display_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'print';
  $context->description = 'Print-optimised output';
  $context->tag = 'layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'pinboard/print' => 'pinboard/print',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'xhtml_print',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Print-optimised output');
  t('layout');
  $export['print'] = $context;

  return $export;
}
